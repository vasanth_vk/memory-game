const gameContainer = document.getElementById("game");
const restartBtn = document.getElementById("restart-btn");
const startBtn = document.getElementById("start-game-btn");
const nameInput = document.getElementById("name-input");
const endScoreDisplay = document.querySelector(".end-score-display");
const endScore = document.getElementById("end-score");
const scoreContainer = document.getElementById("score");
const highScore = document.getElementById("high-score");
const selectContainer = document.getElementById("select-container");
const exitButton = document.getElementById("exit-btn");
const selectOption = document.getElementById("select-option");
const liveScore = document.getElementById("live-score");
const errorMessage = document.getElementById("error-message");

// let errormessage = (document
//     .createElement("p")
//     .classList.add("error-message").innerText = "Please enter your name");

let COLORS = [];

let cradCount = 0;
let cardSelected = [];
let noOfGuesses = 0;
let matched = 0;
let playerName = "";

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors() {
    let colorArray = shuffle(COLORS);
    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over
        newDiv.classList.add("box");
        newDiv.classList.add(color);
        newDiv.style.background = "white";

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}

function getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

// TODO: Implement this function!

function handleCardClick(event) {
    event.preventDefault();

    if (cradCount <= 2) {
        let colorClicked = event.target.classList[1];
        console.log("you clicked", colorClicked);
        cardSelected.push(event.target);
        event.target.style.background = colorClicked;
        event.target.classList.toggle("rotate");
        event.target.style.pointerEvents = "none";
        cradCount += 1;
        event.target.classList.add("active");
    }
    if (cradCount == 2) {
        noOfGuesses += 1;
        scoreContainer.innerText = noOfGuesses;
        let allCards = document.querySelectorAll(".box:not(.active)");
        disablePointerEvents(allCards);

        if (checkMatched(cardSelected) == true) {
            cardSelected[0].classList.add("matched");
            cardSelected[1].classList.add("matched");
            console.log("matched");
            cradCount = 0;
            cardSelected = [];
            enablePointerEvents(allCards);
        } else {
            setTimeout(() => {
                enablePointerEvents(allCards);
                enablePointerEvents(cardSelected);
                if (
                    cardSelected[0] != undefined &&
                    cardSelected[1] != undefined
                ) {
                    cardSelected[0].classList.remove("active");
                    cardSelected[1].classList.remove("active");
                    cardSelected[0].classList.toggle("rotate");
                    cardSelected[1].classList.toggle("rotate");
                    cardSelected[0].style.background = "white";
                    cardSelected[1].style.background = "white";
                    cradCount = 0;
                    cardSelected = [];
                }
            }, 1000);
        }
    }
    if (matched * 2 === COLORS.length) {
        setHighScore(playerName, noOfGuesses);
        setTimeout(() => {
            const cards = document.querySelectorAll(".box");
            cards.forEach((card) => {
                card.parentNode.removeChild(card);
            });
            endScore.innerText = noOfGuesses;
            endScoreDisplay.style.display = "block";
            setTimeout(() => {
                endScoreDisplay.classList.toggle("end-score-active");
            }, 100);
        }, 1000);
    }
}

function checkMatched([card1, card2]) {
    if (card1.classList[1] == card2.classList[1]) {
        matched += 1;
        return true;
    } else {
        return false;
    }
}

function disablePointerEvents(cards) {
    cards.forEach((card) => {
        card.style.pointerEvents = "none";
    });
}

function enablePointerEvents(cards) {
    cards.forEach((card) => {
        card.style.pointerEvents = "auto";
    });
}

function restartGame() {
    cradCount = 0;
    cardSelected = [];
    noOfGuesses = 0;
    matched = 0;
    setTimeout(() => {
        const cards = document.querySelectorAll(".box");
        cards.forEach((card) => {
            card.parentNode.removeChild(card);
        });
    }, 300);
    setTimeout(() => {
        createDivsForColors();
    }, 350);

    // setTimeout(() => {
    // }, 400);

    scoreContainer.innerText = noOfGuesses;
    endScoreDisplay.classList.remove("end-score-active");
    endScoreDisplay.style.display = "none";
}

function setHighScore(player, score) {
    let highScore = JSON.parse(localStorage.getItem("MG_highscore"));
    if (highScore[player] == undefined) {
        highScore = Object.assign(highScore, { [player]: score });
        localStorage.setItem("MG_highscore", JSON.stringify(highScore));
    } else {
        if (score < highScore[player]) {
            highScore = Object.assign(highScore, { [player]: score });
            console.log(highScore);
            localStorage.setItem("MG_highscore", JSON.stringify(highScore));
        }
    }
}

function getHighScore() {
    let highScore = localStorage.getItem("MG_highscore");
    if (highScore == null || highScore == "{}") {
        localStorage.setItem("MG_highscore", JSON.stringify({}));
        return ["none"];
    } else {
        highScore = JSON.parse(highScore);
        return Object.entries(highScore).sort((a, b) => a[1] - b[1])[0];
    }
}

function displayNone(elements) {
    elements.forEach((element) => (element.style.display = "none"));
}

function displayBlock(elements) {
    elements.forEach((element) => (element.style.display = "block"));
}

startBtn.addEventListener("click", () => {
    selectDifficulty();
    playerName = nameInput.value;
    if (nameInput.value == "") {
        nameInput.style.border = "1px solid red";
        errorMessage.innerText = "Please enter name to start...";
    } else {
        errorMessage.innerText = "";
        nameInput.style.border = "1px solid red";
        createDivsForColors();
        displayBlock([restartBtn, exitButton, liveScore]);
        displayNone([nameInput, selectContainer, startBtn, endScoreDisplay]);
    }
});

restartBtn.addEventListener("click", (e) => {
    e.preventDefault();
    restartGame();
});

exitButton.addEventListener("click", () => {
    window.location.reload();
});

selectOption.addEventListener("change", () => {});
function selectDifficulty() {
    COLORS = [];
    let difficultyLevel = { easy: 4, medium: 6, difficult: 8 };
    const noOfCards = difficultyLevel[selectOption.value];
    console.log(noOfCards);
    for (let i = 0; i < noOfCards; i++) {
        COLORS.push(getRandomColor());
    }
    COLORS = [...COLORS, ...COLORS];
}

highScore.innerText = getHighScore().join(" - ");
